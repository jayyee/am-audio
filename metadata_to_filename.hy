(import argparse
        mutagen.mp3
        mutagen.id3
        mutagen.oggvorbis
        mutagen.flac
        mutagen.oggopus
        mutagen.mp4
        mutagen.wavpack
        os
        re)


; Parse directory arg
(setv parser (argparse.ArgumentParser))
(parser.add_argument "directory" :type str)
(setv args (parser.parse_args))
(setv directory args.directory)
(os.chdir directory)

(defn get-file-list [directory]
  (setv working-directory (os.scandir (os.getcwd)))
  (list (map (fn [file]
               file.name)
             (filter (fn [file]
                       (and
                         (file.is_file)
                         (is-music file.name)))
                     working-directory))))

(defn is-music [filename]
  (re.search "\.mp3$|\.opus$|\.flac$|\.wv$|\.ogg$|\.mp4$|\.m4a$" filename))

(defn muta [file]
  (cond
    [(re.search "\.mp3$" file)
     '('(fn [file]
          (when (= (mutagen.mp3.MP3 file) {})
            (setv last_modified (. (os.stat file) st_mtime))
            (setv headless-file (mutagen.id3.ID3FileType file))
            (try
              (.add_tags headless-file)
              (except []))
            (.save headless-file file)
            (os.utime file (, (+ last_modified 1) (+ last_modified 1))))
          (mutagen.mp3.MP3 file))
       '(fn [mFile] (first (first (or (mFile.tags.getall u"TXXX:Rating") '("0")))))
       '(fn [mFile] (round-bpm (first (first (or (mFile.tags.getall u"TBPM") '("0"))))))
       '(fn [mFile] (.upper (first (first (or (mFile.tags.getall u"TKEY") '("0")))))))]
    [(re.search "\.opus$" file)
     '('(fn [file] (mutagen.oggopus.OggOpus file))
       '(fn [mFile] (first (.get mFile.tags "rating" '("0"))))
       '(fn [mFile] (round-bpm (first (.get mFile "bpm" '("0")))))
       '(fn [mFile] (.upper (first (.get mFile "INITIALKEY" '("0"))))))]
    [(re.search "\.flac$" file)
     '('(fn [file] (mutagen.flac.FLAC file))
       '(fn [mFile] (first (.get mFile.tags "rating" '("0"))))
       '(fn [mFile] (round-bpm (first (.get mFile "bpm" '("0")))))
       '(fn [mFile] (.upper (first (.get mFile "INITIALKEY" '("0"))))))]
    [(re.search "\.wv$" file)
     '('(fn [file] (mutagen.wavpack.WavPack file))
       '(fn [mFile] (first (.get mFile.tags "rating" '("0"))))
       '(fn [mFile] (round-bpm (first (.get mFile "bpm" '("0")))))
       '(fn [mFile] (.upper (first (.get mFile "INITIALKEY" '("0"))))))]
    [(re.search "\.ogg$" file)
     '('(fn [file] (mutagen.oggvorbis.OggVorbis file))
       '(fn [mFile] (first (.get mFile.tags "rating" '("0"))))
       '(fn [mFile] (round-bpm (first (.get mFile "bpm" '("0")))))
       '(fn [mFile] (.upper (first (.get mFile "INITIALKEY" '("0"))))))]
    [(re.search "\.mp4$|\.m4a$" file)
     '('(fn [file] (mutagen.mp4.MP4 file))
       '(fn [mFile] (.decode (first (.get mFile.tags "----:com.apple.iTunes:rating" '(b"0"))) "utf-8"))
       '(fn [mFile] (round-bpm (.decode (first (.get mFile.tags "----:com.apple.iTunes:BPM" '(b"0"))) "utf-8")))
       '(fn [mFile] (.upper (.decode (first (.get mFile.tags "----:com.apple.iTunes:initialkey" '(b"0"))) "utf-8"))))]
    [true "some kind of an error"]))

(defn round-bpm [bpm]
  (str (int (float bpm))))

(defn muta-type [muta]
  (eval (first muta)))

(defn muta-get-rating [muta]
  (eval (second muta)))

(defn muta-get-bpm [muta]
  (eval (nth muta 2)))

(defn muta-get-key [muta]
  (eval (nth muta 3)))

(defn get-mutagentype [muta-inst mutFile muta-getter]
  (setv mutMethod ((eval muta-getter) muta-inst))
  (setv attrib ((eval mutMethod) mutFile))
  attrib)

(defn file-metadata [directory]
  (list (map (fn [filename]
               (setv muta-inst (muta filename))
               (setv mutFile ((eval (muta-type muta-inst)) filename))
               {"orig-name" filename
                "rating" (get-mutagentype muta-inst mutFile 'muta-get-rating)
                "bpm" (get-mutagentype muta-inst mutFile 'muta-get-bpm)
                "ikey" (get-mutagentype muta-inst mutFile 'muta-get-key)
                "length" (. (. mutFile info) length)})
             (get-file-list directory))))

(defn updated-name [file]
  (setv orig-name (.get file "orig-name"))
  (setv name-parts (os.path.splitext orig-name))
  (setv rating (or (rating-data (parse-data (first name-parts)))
                   (.get file "rating")))
  (setv bpm (+ " " (.get file "bpm")))
  (setv ikey (+ " " (.get file "ikey")))
  (setv first-name-reset (re.sub "_\[.+\]$" "" (first name-parts)))
  (if (mix-length file)
      (+ first-name-reset "_[" rating "]" (last name-parts))
      (+ first-name-reset "_[" rating bpm ikey "]" (last name-parts))))

(defn mix-length [file]
  (> (.get file "length") 900))

(defn parse-data [title]
  (setv data (re.search "_\[[1-5]\s?\d*\s?\w*\]$" title))
  (if data
      (re.findall "[AB0-9]+" (.group data) 0)
      False))

(defn rating-data [data]
  (if data
      (first data)
      False))

(defn bpm-data [data]
  (if data
      (second data)
      False))

(defn key-data [data]
  (if data
      (last data)
      False))

(defn rename-files [file-list]
  (list (map (fn [file]
               (os.rename (.get file "orig-name")
                          (updated-name file)))
             file-list)))

(rename-files (file-metadata directory))
